{-# OPTIONS --without-K --safe #-}

module Real where

open import Data.Empty
open import Data.Product
open import Data.Rational
open import Data.Rational.Properties
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function
open import Level using () renaming (zero to lzero; suc to lsuc)
open import Relation.Binary
open import Relation.Binary.PropositionalEquality
  using (_≡_; refl; sym)
open import Relation.Unary using (Pred; _∈_; _⊆_)

open import Rational.Bisect
open import Rational.Lemmas

record ℝ : Set₁ where
  field
    lower : Pred ℚ lzero
    upper : Pred ℚ lzero
    lower-inhabited : Σ ℚ lower
    upper-inhabited : Σ ℚ upper
    lower-open : {x : ℚ} → x ∈ lower → Σ ℚ λ y → x < y × y ∈ lower
    upper-open : {y : ℚ} → y ∈ upper → Σ ℚ λ x → x < y × x ∈ upper
    ordered : {x y : ℚ} → x ∈ lower → y ∈ upper → x < y
    located : {x y : ℚ} → x < y → x ∈ lower ⊎ y ∈ upper

  lower-trans : {x y : ℚ} → x ≤ y → y ∈ lower → x ∈ lower
  lower-trans x≤y y∈lower =
    let (z , y<z , z∈lower) = lower-open y∈lower
        x<z = ≤-<-trans x≤y y<z
    in  case located x<z of λ {
          (inj₁ x∈lower) → x∈lower;
          (inj₂ z∈upper) → ⊥-elim (<-irrefl refl (ordered z∈lower z∈upper))
        }

  upper-trans : {x y : ℚ} → x ≤ y → x ∈ upper → y ∈ upper
  upper-trans x≤y x∈upper =
    let (z , z<x , z∈upper) = upper-open x∈upper
        z<y = <-≤-trans z<x x≤y
    in  case located z<y of λ {
          (inj₁ z∈lower) → ⊥-elim (<-irrefl refl (ordered z∈lower z∈upper));
          (inj₂ x∈upper) → x∈upper
        }


open ℝ

infix 4 _◃_

data _◃_ : Rel ℝ (lsuc lzero) where
  lower-⊆ : {x y : ℝ} → lower x ⊆ lower y → x ◃ y

ℚ→ℝ : ℚ → ℝ
ℚ→ℝ a = record
  {
    lower = flip _<_ a;
    upper = _<_ a;
    lower-inhabited = a - 1ℚ ,
      proj₁ <-resp-≡ (proj₂ +-identity a)
                     (+-monoʳ-< a -1ℚ<0ℚ);
    upper-inhabited = a + 1ℚ ,
      proj₂ <-resp-≡ (proj₂ +-identity a)
                     (+-monoʳ-< a 0ℚ<1ℚ);
    lower-open = intermediate;
    upper-open = map id swap ∘ intermediate;
    ordered = <-trans;
    located = λ {x} x<y → case <-cmp x a of λ {
      (tri< x<a _ _) → inj₁ x<a;
      (tri≈ _ x≡a _) → inj₂ (proj₂ <-resp-≡ x≡a x<y);
      (tri> _ _ x>a) → inj₂ (<-trans x>a x<y)
    }
  }
