{-# OPTIONS --without-K #-}

module Real.Addition where

open import Algebra using (CommutativeRing)
open import Data.Product
open import Data.Rational
open import Data.Rational.Properties
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Function using (_∘_; case_of_)
open import Level using () renaming (zero to lzero)
open import Relation.Binary.PropositionalEquality
open import Relation.Unary using (Pred; _∈_)

open import Rational.Bisect using (intermediate)
open import Rational.Lemmas

open import Real
open import Real.Approx

open import Algebra.Properties.Ring (CommutativeRing.ring +-*-commutativeRing)
  using (-‿involutive)

open import CommutativeRing.Identities +-*-commutativeRing

open ℝ

infix  8 ⊖_
infixl 6 _⊕_ _⊖_

private abstract
  intermediate-abstract : {a b : ℚ} → a < b → Σ ℚ λ c → a < c × c < b
  intermediate-abstract = intermediate

_⊕_ : ℝ → ℝ → ℝ
z₁ ⊕ z₂ = record
  {
    lower = ⊕-lower ;
    upper = ⊕-upper ;
    lower-inhabited = ⊕-lower-inhabited ;
    upper-inhabited = ⊕-upper-inhabited ;
    lower-open = ⊕-lower-open ;
    upper-open = ⊕-upper-open ;
    ordered = ⊕-ordered ;
    located = ⊕-located
  } where
  ⊕-lower : Pred ℚ lzero
  ⊕-lower x = Σ ℚ λ x₁ → lower z₁ x₁ × Σ ℚ λ x₂ → lower z₂ x₂ × x₁ + x₂ ≡ x
  ⊕-upper : Pred ℚ lzero
  ⊕-upper y = Σ ℚ λ y₁ → upper z₁ y₁ × Σ ℚ λ y₂ → upper z₂ y₂ × y₁ + y₂ ≡ y
  ⊕-lower-inhabited : Σ ℚ ⊕-lower
  ⊕-lower-inhabited =
    let (x₁ , x₁<z₁) = lower-inhabited z₁
        (x₂ , x₂<z₂) = lower-inhabited z₂
    in  x₁ + x₂ , x₁ , x₁<z₁ , x₂ , x₂<z₂ , refl
  ⊕-upper-inhabited : Σ ℚ ⊕-upper
  ⊕-upper-inhabited =
    let (y₁ , y₁>z₁) = upper-inhabited z₁
        (y₂ , y₂>z₂) = upper-inhabited z₂
    in  y₁ + y₂ , y₁ , y₁>z₁ , y₂ , y₂>z₂ , refl
  ⊕-lower-open : {x : ℚ} → x ∈ ⊕-lower → Σ ℚ λ y → x < y × y ∈ ⊕-lower
  ⊕-lower-open (x₁ , x₁<z₁ , x₂ , x₂<z₂ , x≡x₁+x₂) =
    let (y₁ , x₁<y₁ , y₁<z₁) = lower-open z₁ x₁<z₁
        (y₂ , x₂<y₂ , y₂<z₂) = lower-open z₂ x₂<z₂
        x₁+x₂<y₁+y₂ = +-mono-< x₁<y₁ x₂<y₂
        x<y₁+y₂ = proj₂ <-resp-≡ x≡x₁+x₂ x₁+x₂<y₁+y₂
    in  y₁ + y₂ , x<y₁+y₂ , y₁ , y₁<z₁ , y₂ , y₂<z₂ , refl
  ⊕-upper-open : {y : ℚ} → y ∈ ⊕-upper → Σ ℚ λ x → x < y × x ∈ ⊕-upper
  ⊕-upper-open (y₁ , y₁>z₁ , y₂ , y₂>z₂ , y≡y₁+y₂) =
    let (x₁ , x₁<y₁ , x₁>z₁) = upper-open z₁ y₁>z₁
        (x₂ , x₂<y₂ , x₂>z₂) = upper-open z₂ y₂>z₂
        x₁+x₂<y₁+y₂ = +-mono-< x₁<y₁ x₂<y₂
        x₁+x₂<y = proj₁ <-resp-≡ y≡y₁+y₂ x₁+x₂<y₁+y₂
    in  x₁ + x₂ , x₁+x₂<y , x₁ , x₁>z₁ , x₂ , x₂>z₂ , refl
  ⊕-ordered : {x y : ℚ} → x ∈ ⊕-lower → y ∈ ⊕-upper → x < y
  ⊕-ordered (x₁ , x₁<z₁ , x₂ , x₂<z₂ , x≡x₁+x₂)
            (y₁ , y₁>z₁ , y₂ , y₂>z₂ , y≡y₁+y₂) =
    let x₁<y₁ = ordered z₁ x₁<z₁ y₁>z₁
        x₂<y₂ = ordered z₂ x₂<z₂ y₂>z₂
        x₁+x₂<y₁+y₂ = +-mono-< x₁<y₁ x₂<y₂
    in  proj₁ <-resp-≡ y≡y₁+y₂ (proj₂ <-resp-≡ x≡x₁+x₂ x₁+x₂<y₁+y₂)
  ⊕-located : {x y : ℚ} → x < y → x ∈ ⊕-lower ⊎ y ∈ ⊕-upper
  ⊕-located {x} {y} x<y =
    let ε = y - x
        ε>0 = x<y⇒0<y-x x<y
        (δ , δ>0 , δ<ε) = intermediate-abstract ε>0
        ((u , v) , v-u≤δ , u<z₂ , v>z₂) = approx z₂ δ (positive δ>0)
        v-u<y-x = ≤-<-trans v-u≤δ δ<ε
        open ≤-Reasoning
        x-u<y-v : x - u < y - v
        x-u<y-v = begin-strict
          x - u           ≡˘⟨ [x-y]+[y+z]≈x+z x v (- u) ⟩
          x - v + (v - u) <⟨ +-monoʳ-< (x - v) v-u<y-x ⟩
          x - v + (y - x) ≡⟨ +-comm (x - v) (y - x) ⟩
          y - x + (x - v) ≡⟨ [x-y]+[y+z]≈x+z y x (- v) ⟩
          y - v           ∎
    in  case located z₁ x-u<y-v of λ {
      (inj₁ x-u<z₁) → inj₁ (x - u , x-u<z₁ , u , u<z₂ , x-y+y≈x x u) ;
      (inj₂ y-v>z₁) → inj₂ (y - v , y-v>z₁ , v , v>z₂ , x-y+y≈x y v)
    }

⊖_ : ℝ → ℝ
⊖_ z = record
  {
    lower = λ x → upper z (- x);
    upper = λ y → lower z (- y);
    lower-inhabited =
      let (b , b∈upper) = upper-inhabited z
      in  - b , subst (upper z) (x≡--x b) b∈upper;
    upper-inhabited =
      let (b , b∈lower) = lower-inhabited z
      in  - b , subst (lower z) (x≡--x b) b∈lower;
    lower-open = λ {x} x∈lower →
      let (y , y<-x , y∈upper) = upper-open z x∈lower
          x<-y = proj₂ <-resp-≡ (-‿involutive x) (neg-antimono-< y<-x)
      in  - y , x<-y , subst (upper z) (x≡--x y) y∈upper;
    upper-open = λ {y} y∈upper →
      let (x , -y<x , x∈lower) = lower-open z y∈upper
          -x<y = proj₁ <-resp-≡ (-‿involutive y) (neg-antimono-< -y<x)
      in  - x , -x<y , subst (lower z) (x≡--x x) x∈lower;
    ordered = λ {x} {y} x∈lower y∈upper →
      let -y<-x = ordered z y∈upper x∈lower
      in  proj₂ <-resp-≡ (-‿involutive x)
                (proj₁ <-resp-≡ (-‿involutive y) (neg-antimono-< -y<-x));
    located = λ x<y →
      let -y<-x = neg-antimono-< x<y
      in  case located z -y<-x of λ {
            (inj₁ -y∈lower) → inj₂ -y∈lower;
            (inj₂ -x∈upper) → inj₁ -x∈upper
          }
  }
  where
    x≡--x : (x : ℚ) → x ≡ - - x
    x≡--x = sym ∘ -‿involutive

_⊖_ : ℝ → ℝ → ℝ
z₁ ⊖ z₂ = z₁ ⊕ ⊖ z₂
