{-# OPTIONS --without-K #-}

module Real.Approx where

open import Algebra using (CommutativeRing)
open import Data.Integer using (ℤ; +_)
open import Data.Nat using (ℕ; suc)
open import Data.Product
open import Data.Rational
open import Data.Rational.Properties
open import Data.Sum
open import Function
open import Relation.Binary.PropositionalEquality
  using (_≡_; cong; sym; module ≡-Reasoning)
open import Relation.Unary using (_∈_)

open import Rational.Bisect
open import Rational.Exp
open import Rational.Lemmas
open import Real

open CommutativeRing +-*-commutativeRing using (+-cong)

open import CommutativeRing.Identities +-*-commutativeRing
  using ([x-y]+[y+z]≈x+z)

open ℝ

private

  exp₂>0 : ℤ → Σ ℚ λ x → Positive x
  exp₂>0 n = exp>0 2ℚ _ n

  2^ : ℤ → ℚ
  2^ n = proj₁ (exp₂>0 n)

  abstract
    bisect-abstract : {a b : ℚ} → a < b →
                      Σ ℚ λ c → a < c × c < b ×
                      2ℚ * (c - a) ≡ b - a × 2ℚ * (b - c) ≡ b - a
    bisect-abstract = bisect

  step : (x : ℝ) → (a b : ℚ) → a ∈ lower x → b ∈ upper x →
         Σ (ℚ × ℚ) λ { (a′ , b′) → 2ℚ * (b′ - a′) ≡ b - a
                                   × a′ ∈ lower x × b′ ∈ upper x }
  step x a b a∈lower b∈upper =
    let d = b - a
        a<b = ordered x a∈lower b∈upper
        (s , a<s , s<b , 2[s-a]≡d , 2[b-s]≡d) = bisect-abstract a<b
        (r , _ , r<s , _ , 2[s-r]≡s-a) = bisect a<s
        (t , s<t , _ , 2[t-s]≡b-s , _) = bisect s<b
        2[t-r]≡d : 2ℚ * (t - r) ≡ d
        2[t-r]≡d = begin
          2ℚ * (t - r)                ≡⟨ cong (2ℚ *_)
                                         (sym ([x-y]+[y+z]≈x+z t s (- r))) ⟩
          2ℚ * (t - s + (s - r))      ≡⟨ *-distribˡ-+ 2ℚ (t - s) (s - r) ⟩
          2ℚ * (t - s) + 2ℚ * (s - r) ≡⟨ +-cong 2[t-s]≡b-s 2[s-r]≡s-a ⟩
          b - s + (s - a)             ≡⟨ [x-y]+[y+z]≈x+z b s (- a) ⟩
          b - a                       ∎
    in  case located x r<s , located x s<t of λ {
          (inj₂ s∈upper , _) → (a , s) , 2[s-a]≡d , a∈lower , s∈upper;
          (inj₁ r∈lower , inj₂ t∈upper) → (r , t) , 2[t-r]≡d , r∈lower , t∈upper;
          (_ , inj₁ s∈lower) → (s , b) , 2[b-s]≡d , s∈lower , b∈upper
        }
    where open ≡-Reasoning

  iterate : (x : ℝ) → (a₀ b₀ : ℚ) → (n : ℕ) → a₀ ∈ lower x → b₀ ∈ upper x →
         Σ (ℚ × ℚ) λ { (a , b) → 2^ (+ n) * (b - a) ≡ b₀ - a₀
                                 × a ∈ lower x × b ∈ upper x }
  iterate x a₀ b₀ 0 a₀∈lower b₀∈upper =
    (a₀ , b₀) , proj₁ *-identity (b₀ - a₀) , a₀∈lower , b₀∈upper
  iterate x a₀ b₀ (suc n) a₀∈lower b₀∈upper =
    let ((a₁ , b₁) , eq₁ , a₁∈lower , b₁∈upper) = iterate x a₀ b₀ n a₀∈lower b₀∈upper
        ((a , b) , eq , a∈lower , b∈upper) = step x a₁ b₁ a₁∈lower b₁∈upper
        eq′ : 2^ (+ suc n) * (b - a) ≡ b₀ - a₀
        eq′ = begin
          2^ (+ suc n) * (b - a)     ≡⟨ *-assoc (2^ (+ n)) 2ℚ (b - a) ⟩
          2^ (+ n) * (2ℚ * (b - a)) ≡⟨ cong (2^ (+ n) *_) eq ⟩
          2^ (+ n) * (b₁ - a₁)       ≡⟨ eq₁ ⟩
          b₀ - a₀                    ∎
    in  (a , b) , eq′ , a∈lower , b∈upper
    where open ≡-Reasoning

  {-# TERMINATING #-}
  log : (x ε : ℚ) → Positive ε →
        Σ ℕ λ n → x ≤ 2^ (+ n) * ε
  log x ε 0<ε with x<y∨y≤x ε x
  ... | inj₁ ε<x =
    let 0<2ε = *-pos 2ℚ ε _ 0<ε
        (n , x≤2ⁿ[2ε]) = log x (2ℚ * ε) 0<2ε
        2ⁿ[2ε]≡2¹⁺ⁿε = sym (*-assoc (2^ (+ n)) 2ℚ ε)
    in  suc n , ≤-trans x≤2ⁿ[2ε] (≤-reflexive 2ⁿ[2ε]≡2¹⁺ⁿε)
  ... | inj₂ x≤ε = 0 , ≤-trans x≤ε (≤-reflexive (sym (proj₁ *-identity ε)))

approx : (x : ℝ) → (ε : ℚ) → Positive ε →
         Σ (ℚ × ℚ) λ { (a , b) → b - a ≤ ε × a ∈ lower x × b ∈ upper x }
approx x ε 0<ε =
  let (a₀ , a₀∈lower) = lower-inhabited x
      (b₀ , b₀∈upper) = upper-inhabited x
      d = b₀ - a₀
      (n , d≤2ⁿε) = log d ε 0<ε
      2ⁿ , 0<2ⁿ = exp₂>0 (+ n)
      ((a , b) , 2ⁿ[b-a]≡d , a∈lower , b∈upper) = iterate x a₀ b₀ n a₀∈lower b₀∈upper
      2ⁿ[b-a]≤2ⁿε = ≤-trans (≤-reflexive 2ⁿ[b-a]≡d) d≤2ⁿε
      b-a≤ε = *-cancelˡ-≤-pos 2ⁿ 0<2ⁿ 2ⁿ[b-a]≤2ⁿε
  in  (a , b) , b-a≤ε , a∈lower , b∈upper
