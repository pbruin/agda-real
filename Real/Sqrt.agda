{-# OPTIONS --without-K --safe #-}

module Real.Sqrt where

open import Data.Empty
open import Data.Integer using (+_; -[1+_])
open import Data.Product
open import Data.Rational as ℚ
open import Data.Rational.Properties
open import Data.Sum
open import Function
open import Level using (zero)
open import Relation.Binary.PropositionalEquality
open import Relation.Unary using (Pred; _∈_; _⊆_)

open import Rational.Bisect
open import Rational.Lemmas

open import Real
open import Real.SqrtLemmas

-- Note: by the definition below, the square root of a negative real
-- number equals 0.
√ : (z : ℝ) → ℝ
√ z = record
  {
    lower = √z-lower;
    upper = √z-upper;
    lower-inhabited = √z-lower-inhabited;
    upper-inhabited = √z-upper-inhabited;
    lower-open = √z-lower-open;
    upper-open = √z-upper-open;
    ordered = √z-ordered;
    located = √z-located
  }
  where
    open ℝ z

    √z-lower : Pred ℚ zero
    √z-lower x = ℚ.NonNegative x → x * x ∈ lower

    √z-upper : Pred ℚ zero
    √z-upper y = ℚ.Positive y × y * y ∈ upper

    √z-lower-inhabited : Σ ℚ √z-lower
    √z-lower-inhabited = - 1ℚ , λ ()

    √z-upper-inhabited : Σ ℚ √z-upper
    √z-upper-inhabited =
      let (y , y∈upper) = upper-inhabited
          x = 1ℚ ⊔ y
          1≤x = p≤p⊔q 1ℚ y
          y≤x = p≤q⊔p 1ℚ y
          x-pos = positive (<-≤-trans 0ℚ<1ℚ 1≤x)
          x≤x² = 1≤x⇒x≤x² 1≤x
          y≤x² = ≤-trans y≤x x≤x²
          x²∈upper = upper-trans y≤x² y∈upper
      in  x , x-pos , x²∈upper

    √z-lower-open : {x : ℚ} → x ∈ √z-lower → Σ ℚ λ y → x < y × y ∈ √z-lower
    √z-lower-open x@{mkℚ -[1+ _ ] _ _} _ =
      let (y , x<y , y<0) = intermediate (negative⁻¹ _)
      in  (y , x<y , λ r → ⊥-elim (lemma y r (negative y<0))) where
        lemma : (t : ℚ) → ℚ.NonNegative t → ℚ.Negative t → ⊥
        lemma (mkℚ (+ _) _ _) _ ()
    √z-lower-open x@{mkℚ (+ _) _ _} p→x²∈lower =
      let x²∈lower = p→x²∈lower _
          (b , x²<b , b∈lower) = lower-open x²∈lower
          (y , x<y , y²≤b) = lemma-lower b x x²<b
          y²∈lower = lower-trans y²≤b b∈lower
      in  y , x<y , λ _ → y²∈lower

    √z-upper-open : {y : ℚ} → y ∈ √z-upper → Σ ℚ λ x → x < y × x ∈ √z-upper
    √z-upper-open y@{mkℚ +[1+ _ ] _ _} (_ , y²∈upper) =
      let (a , a<y² , a∈upper) = upper-open y²∈upper
          (x , x-pos , x<y , a≤x²) = lemma-upper a y a<y²
          x²∈upper = upper-trans a≤x² a∈upper
      in  (x , x<y , x-pos , x²∈upper)

    √z-ordered : {x y : ℚ} → x ∈ √z-lower → y ∈ √z-upper → x < y
    √z-ordered x@{mkℚ -[1+ _ ] _ _} y@{mkℚ +[1+ _ ] _ _} _ _ =
      negative<positive _ _
    √z-ordered x@{mkℚ (+ _) _ _} y@{mkℚ +[1+ _ ] _ _} p→x²∈lower (_ , y²∈upper) =
      let x²∈lower = p→x²∈lower _
          x²<y² = ordered x²∈lower y²∈upper
      in  sqrt-mono-< (nonNegative⁻¹ _) x²<y²

    √z-located : {x y : ℚ} → x < y → x ∈ √z-lower ⊎ y ∈ √z-upper
    √z-located x@{mkℚ -[1+ _ ] _ _} _ = inj₁ (λ ())
    √z-located x@{mkℚ (+ _) _ _} x<y =
      let 0≤x = nonNegative⁻¹ _
          x²<y² = 0≤x<y⇒x²<y² 0≤x x<y
          y-pos = positive (≤-<-trans 0≤x x<y)
      in  case located x²<y² of λ {
            (inj₁ x²∈lower) → inj₁ (λ _ → x²∈lower);
            (inj₂ y²∈upper) → inj₂ (y-pos , y²∈upper)
          }

private

  √2 : ℝ
  √2 = √ (ℚ→ℝ 2ℚ)
