{-# OPTIONS --without-K --safe #-}

module Real.SqrtLemmas where

open import Algebra using (CommutativeRing)
import Data.Integer as ℤ
open import Data.Product
open import Data.Rational
open import Data.Rational.Properties
open import Relation.Binary.PropositionalEquality

open import Rational.Bisect
open import Rational.Lemmas

open CommutativeRing +-*-commutativeRing using (+-cong; -‿inverse; distrib)

open import Algebra.Properties.Ring (CommutativeRing.ring +-*-commutativeRing)
  using (-‿distribˡ-*)

open import CommutativeRing.Identities +-*-commutativeRing

1≤x⇒x≤x² : {x : ℚ} → 1ℚ ≤ x → x ≤ x * x
1≤x⇒x≤x² {x} 1≤x = begin
  x      ≡⟨ sym (proj₁ *-identity x) ⟩
  1ℚ * x ≤⟨ *-monoʳ-≤-pos x (positive (<-≤-trans 0ℚ<1ℚ 1≤x)) 1≤x ⟩
  x * x  ∎
  where open ≤-Reasoning

0≤x≤1⇒x²≤x : {x : ℚ} → 0ℚ ≤ x → x ≤ 1ℚ → x * x ≤ x
0≤x≤1⇒x²≤x {x} 0≤x x≤1 = begin
  x * x  ≤⟨ *-monoʳ-≤-nonNeg x (nonNegative 0≤x) x≤1 ⟩
  1ℚ * x ≡⟨ proj₁ *-identity x ⟩
  x      ∎
  where open ≤-Reasoning

0≤x≤y⇒x²≤y² : {x y : ℚ} → 0ℚ ≤ x → x ≤ y → x * x ≤ y * y
0≤x≤y⇒x²≤y² {x} {y} 0≤x x≤y = begin
  x * x ≤⟨ *-monoʳ-≤-nonNeg x (nonNegative 0≤x) x≤y ⟩
  y * x ≤⟨ *-monoˡ-≤-nonNeg y (nonNegative (≤-trans 0≤x x≤y)) x≤y ⟩
  y * y ∎
  where open ≤-Reasoning

0≤x<y⇒x²<y² : {x y : ℚ} → 0ℚ ≤ x → x < y → x * x < y * y
0≤x<y⇒x²<y² {x} {y} 0≤x x<y =
  let 0<y = ≤-<-trans 0≤x x<y
      x²≤yx = *-monoʳ-≤-nonNeg x (nonNegative 0≤x) (<⇒≤ x<y)
      yx<y² = *-monoʳ-<-pos y (positive 0<y) x<y
  in  ≤-<-trans x²≤yx yx<y²

private

  expand : (x ε : ℚ) → (x + ε) * (x + ε) ≡ x * x + (ε * (x + x) + ε * ε)
  expand x ε =
    let x² = x * x
        εx = ε * x
        ε² = ε * ε
    in  begin
          (x + ε) * (x + ε)         ≡⟨ proj₁ distrib (x + ε) x ε ⟩
          (x + ε) * x + (x + ε) * ε ≡⟨ +-cong (proj₂ distrib x x ε)
                                       (proj₂ distrib ε x ε) ⟩
          x² + εx + (x * ε + ε²)    ≡⟨ +-cong {x² + εx} refl
                                       (+-cong {v = ε²} (*-comm x ε) refl) ⟩
          x² + εx + (εx + ε²)       ≡⟨ +-assoc x² εx (εx + ε²) ⟩
          x² + (εx + (εx + ε²))     ≡⟨ +-cong {x²} refl
                                       (sym (+-assoc εx εx ε²)) ⟩
          x² + (εx + εx + ε²)       ≡⟨ +-cong {x²} refl
                                       (+-cong {v = ε²}
                                        (sym (proj₁ distrib ε x x)) refl) ⟩
          x² + (ε * (x + x) + ε²)   ∎
    where open ≡-Reasoning

  expand′ : (x ε : ℚ) → (x + ε) * (x + ε) ≡ x * x + ε * (x + x + ε)
  expand′ x ε = begin
    (x + ε) * (x + ε)             ≡⟨ *-distribʳ-+ (x + ε) x ε ⟩
    x * (x + ε) + ε * (x + ε)     ≡⟨ cong (_+ ε * (x + ε)) (*-distribˡ-+ x x ε) ⟩
    x * x + x * ε + ε * (x + ε)   ≡⟨ cong (λ y → x * x + y + ε * (x + ε)) (*-comm x ε) ⟩
    x * x + ε * x + ε * (x + ε)   ≡⟨ +-assoc (x * x) (ε * x) (ε * (x + ε)) ⟩
    x * x + (ε * x + ε * (x + ε)) ≡⟨ cong (x * x +_) (sym (*-distribˡ-+ ε x (x + ε))) ⟩
    x * x + ε * (x + (x + ε))     ≡⟨ cong (λ y → x * x + ε * y) (sym (+-assoc x x ε)) ⟩
    x * x + ε * (x + x + ε)       ∎
    where open ≡-Reasoning

lemma-lower : (b x : ℚ) → .{NonNegative x} → x * x < b →
              Σ ℚ λ y → x < y × y * y ≤ b
lemma-lower b x@(mkℚ (ℤ.+ _) _ _) x²<b =
  let x² = x * x
      d = b - x²
      d-pos = positive (x<y⇒0<y-x x²<b)
      2x = x + x
      2x-nonNeg = +-nonNeg x x _ _
      2x+1 = 2x + 1ℚ
      2x+1‿pos = +-nonNeg-pos (x + x) 1ℚ 2x-nonNeg _
      (h , h-pos , h[2x+1]≡d) = divide-pos d 2x+1 d-pos 2x+1‿pos
      ε = 1ℚ ⊓ h
      ε≤1 = p⊓q≤p 1ℚ h
      ε≤h = p⊓q≤q 1ℚ h
      ε-pos = ⊓-pos 1ℚ h _ h-pos
      y = x + ε
      x<y = 0<x⇒y<y+x {ε} {x} (positive⁻¹ ε-pos)

      ε[2x+ε]≤d : ε * (2x + ε) ≤ d
      ε[2x+ε]≤d = begin
        ε * (2x + ε) ≤⟨ *-monoˡ-≤-pos ε ε-pos (+-monoʳ-≤ 2x ε≤1) ⟩
        ε * 2x+1     ≤⟨ *-monoʳ-≤-pos 2x+1 2x+1‿pos ε≤h ⟩
        h * 2x+1     ≡⟨ h[2x+1]≡d ⟩
        d            ∎

      y²≤b : y * y ≤ b
      y²≤b = begin
        (x + ε) * (x + ε) ≡⟨ expand′ x ε ⟩
        x² + ε * (2x + ε) ≤⟨ +-monoʳ-≤ x² ε[2x+ε]≤d ⟩
        x² + d            ≡⟨ cong (x² +_) (+-comm b (- x²)) ⟩
        x² + (- x² + b)   ≡⟨ sym (+-assoc x² (- x²) b) ⟩
        x² - x² + b       ≡⟨ cong (_+ b) (+-inverseʳ x²) ⟩
        0ℚ + b            ≡⟨ +-identityˡ b ⟩
        b                 ∎
  in  y , x<y , y²≤b
  where open ≤-Reasoning

lemma-upper : (a y : ℚ) → .{Positive y} → a < y * y →
              Σ ℚ λ x → Positive x × x < y × a ≤ x * x
lemma-upper a y@(mkℚ +[1+ _ ] _ _) a<y² =
  let y² = y * y
      d = y² - a
      d-pos = positive (x<y⇒0<y-x a<y²)
      2y = y + y
      2y-pos = +-pos y y _ _
      (r , 0<r , r<y) = intermediate (positive⁻¹ {y} _)
      (d/2y , d/2y-pos , [d/2y]2y≡d) = divide-pos d 2y d-pos 2y-pos
      ε = r ⊓ d/2y
      ε≤r = p⊓q≤p r d/2y
      ε≤d/2y = p⊓q≤q r d/2y
      ε-pos = ⊓-pos r d/2y (positive 0<r) d/2y-pos
      ε² = (- ε) * (- ε)
      ε<y = ≤-<-trans ε≤r r<y
      x = y - ε
      x-pos = positive (x<y⇒0<y-x ε<y)
      x<y = 0<x⇒y-x<y (positive⁻¹ ε-pos)

      ε[2y]≤d : ε * 2y ≤ d
      ε[2y]≤d = begin
        ε * 2y    ≤⟨ *-monoʳ-≤-pos 2y 2y-pos ε≤d/2y ⟩
        d/2y * 2y ≡⟨ [d/2y]2y≡d ⟩
        d         ∎

      a≤x² : a ≤ x * x
      a≤x² = begin
        a                      ≡⟨ sym (x-[x-y]≈y y² a) ⟩
        y² - d                 ≤⟨ +-monoʳ-≤ y² (neg-antimono-≤ ε[2y]≤d) ⟩
        y² - ε * 2y            ≡⟨ sym (proj₂ +-identity (y² - ε * 2y)) ⟩
        y² - ε * 2y + 0ℚ       ≤⟨ +-monoʳ-≤ (y² - ε * 2y) (0≤x² (- ε)) ⟩
        y² - ε * 2y + ε²       ≡⟨ +-assoc y² (- (ε * 2y)) ε² ⟩
        y² + (- (ε * 2y) + ε²) ≡⟨ +-cong {y²} refl
                                  (+-cong {v = ε²} (-‿distribˡ-* ε 2y) refl) ⟩
        y² + (- ε * 2y + ε²)   ≡⟨ sym (expand y (- ε)) ⟩
        x * x                  ∎
  in  x , x-pos , x<y , a≤x²
  where open ≤-Reasoning

sqrt-mono-< : {x y : ℚ} → 0ℚ ≤ y → x * x < y * y → x < y
sqrt-mono-< {x} {y} 0≤y x²<y² = case x<y∨y≤x x y of λ {
    (inj₁ x<y) → x<y;
    (inj₂ y≤x) →
      let y²≤x² = 0≤x≤y⇒x²≤y² 0≤y y≤x
          x²<x² = <-≤-trans x²<y² y²≤x²
      in  ⊥-elim (<-irrefl refl x²<x²)
  }
  where
    open import Data.Empty
    open import Data.Sum
    open import Function
