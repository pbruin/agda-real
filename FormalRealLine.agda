{-# OPTIONS --without-K --safe #-}

-- The real numbers as a formal topology

module FormalRealLine where

open import Data.Product
open import Data.Rational
open import Data.Rational.Properties
open import Data.Sum using (_⊎_; inj₁; inj₂)
open import Data.Unit using (⊤)
open import Function using (_∘_; flip; case_of_)
open import Level using (zero)
open import Relation.Binary
open import Relation.Binary.PropositionalEquality
  using (_≡_; refl; isEquivalence)
open import Relation.Unary using (Pred; _∈_)

open import Rational.Bisect
open import Rational.Lemmas

-- The basic open sets are (possibly empty) bounded open intervals.

S : Set zero
S = ℚ × ℚ

_≤S_ : Rel S zero
(p , q) ≤S (r , s) = (r ≤ p) × (q ≤ s)

≤S-refl : _≡_ ⇒ _≤S_
≤S-refl refl = (≤-refl , ≤-refl)

≤S-refl′ : Reflexive _≤S_
≤S-refl′ = ≤S-refl refl

≤S-trans : Transitive _≤S_
≤S-trans = zip (flip ≤-trans) ≤-trans

isPreorder : IsPreorder _≡_ _≤S_
isPreorder = record
  {
    isEquivalence = isEquivalence;
    reflexive = ≤S-refl;
    trans = ≤S-trans
  }

open import Topology.IndGenTop (record { isPreorder = isPreorder }) zero zero
  hiding (S; ≤-refl; ≤-trans)

-- An open interval (p , q) has two types of basic open coverings:
-- (1) for any pair (r , s) of rational numbers with r < s, the
--     two-element cover { (p , s) , (r , q) };
-- (2) the cover { (x , y) | p < x < y < q } (which is infinite
--     if the interval (p , q) is non-empty).

I : S → Set zero
I _ = Σ S (uncurry _<_) ⊎ ⊤

C : (a : S) → I a → 𝕌
C (p , q) (inj₁ ((r , s) , r<s)) a = a ≡ (p , s) ⊎ a ≡ (r , q)
C (p , q) (inj₂ t) (x , y) = p < x × x < y × y < q

-- An open interval (p , q) is positive if and only if it is
-- non-empty.

Pos : Pred S zero
Pos (p , q) = p < q

Pos′ : Pred 𝕌 zero
Pos′ U = Σ S λ b → b ∈ U × Pos b

-- Every non-empty open interval contains a rational number.

split₂ : {p q : ℚ} → p < q → Σ S λ { (r , s) → p < r × r < s × s < q }
split₂ p<q =
  let (r , p<r , r<q) = intermediate p<q
      (s , r<s , s<q) = intermediate r<q
  in  ((r , s) , p<r , r<s , s<q)

-- Verification of the axioms for an inductively generated topology

-- An open interval containing a non-empty interval is non-empty.

isMonotone : Pos Respects _≤S_
isMonotone (r≤p , q≤s) p<q = ≤-<-trans r≤p (<-≤-trans p<q q≤s)

-- Given a non-empty open interval (p , q) and a basic open covering
-- of some open interval (r , s) containing (p , q), there exists a
-- non-empty open interval contained both in (p , q) and in one of the
-- covering intervals.

isMonotone′ : {a b : S} →
  (i : I b) → a ≤S b → Pos a → Pos′ ((C b i) ↓′ a)

isMonotone′ {(p , q)} {(r , s)} (inj₁ ((x , y) , x<y)) (r≤p , q≤s) p<q =
  case x<y∨y≤x p y of λ {
    (inj₁ p<y) → case x<y∨y≤x q y of λ {
      (inj₁ q<y) → (p , q) , (((r , y) , inj₁ refl ,
                               (r≤p , <⇒≤ q<y)) , ≤S-refl′) , p<q;
      (inj₂ y≤q) → (p , y) , (((r , y) , inj₁ refl ,
                               (r≤p , ≤-refl)) , (≤-refl , y≤q)) , p<y
      };
    (inj₂ y≤p) → let x≤p = ≤-trans (<⇒≤ x<y) y≤p
                 in (p , q) ,
                    (((x , s) , inj₂ refl , (x≤p , q≤s)) ,
                      ≤S-refl′) , p<q
  }

isMonotone′ (inj₂ t) (r≤p , q≤s) p<q =
  let (c@(u , v) , p<u , u<v , v<q) = split₂ p<q
      p≤u = <⇒≤ p<u
      v≤q = <⇒≤ v<q
      r<u = ≤-<-trans r≤p p<u
      v<s = <-≤-trans v<q q≤s
  in  c , ((c , (r<u , u<v , v<s) , ≤S-refl′) , p≤u , v≤q) , u<v

IndGenRealLine : IndGenTop
IndGenRealLine = record
  {
    monotonicity-≤ = isMonotone;
    monotonicity-≤-infinity = isMonotone′
  }

FormalRealLine : FormalTopology
FormalRealLine = IndGenTop.formalTopology IndGenRealLine
