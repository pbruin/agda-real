{-# OPTIONS --without-K --safe #-}

-- T. Coquand, G. Sambin, J. Smith and S. Valentini,
-- Inductively generated formal topologies

open import Data.Product using (Σ; _×_; -,_; _,_)
open import Function using (_∘_)
open import Level using (Level; suc; _⊔_)
open import Relation.Binary using (Preorder; REL; Rel)
open import Relation.Unary using (Pred; _∈_; _∩_; _⊆_)

module Topology.IndGenTop {ℓ : Level} (preorder : Preorder ℓ ℓ ℓ)
  (ι π : Level) where

open import Topology.FormalTop preorder (suc ℓ ⊔ ι ⊔ π) π public

record IndGenTop : Set (suc (ℓ ⊔ ι ⊔ π)) where
  infix 4 _◃_
  infix 4 _◂_

  field
    I : S → Set ι
    C : (a : S) → I a → 𝕌
    Pos : Pred S π

  Pos′ : Pred 𝕌 (ℓ ⊔ π)
  Pos′ U = Σ S λ b → b ∈ U × Pos b

  field
    monotonicity-≤ : {a b : S} → a ≤ b → Pos a → Pos b
    monotonicity-≤-infinity : {a b : S} →
      (i : I b) → a ≤ b → Pos a → Pos′ ((C b i) ↓′ a)

  data _◃_ : REL S 𝕌 (suc ℓ ⊔ ι ⊔ π) where
    reflexivity : {a : S} {U : 𝕌} →
      a ∈ U → a ◃ U
    ≤-left : {a b : S} {U : 𝕌} →
      a ≤ b → b ◃ U → a ◃ U
    ≤-infinity : {a b : S} {i : I b} {U : 𝕌} →
      a ≤ b → ({x : S} → x ∈ (C b i) ↓′ a → x ◃ U) → a ◃ U
    positivity : {a : S} {U : 𝕌} →
      (Pos a → a ◃ U) → a ◃ U

  a◃a↓a : {a : S} → a ◃ a ↓ a
  a◃a↓a = let a≤a = ≤-refl in reflexivity (a≤a , a≤a)

  _◂_ : Rel 𝕌 (suc ℓ ⊔ ι ⊔ π)
  U ◂ V = {x : S} → x ∈ U → x ◃ V

  ◂-mono : {U V W : 𝕌} →
    U ⊆ V → V ◂ W → U ◂ W
  ◂-mono U⊆V V◂W = V◂W ∘ U⊆V

  ◃-trans : {a : S} {U V : 𝕌} →
    a ◃ U → U ◂ V → a ◃ V
  ◃-trans (reflexivity a∈U) U◂V = U◂V a∈U
  ◃-trans (≤-left a≤b b◃U) U◂V = ≤-left a≤b (◃-trans b◃U U◂V)
  ◃-trans (≤-infinity {i = i} a≤b P) U◂V =
    ≤-infinity {i = i} a≤b (λ p → ◃-trans (P p) U◂V)
  ◃-trans (positivity P) U◂V = positivity (λ p → ◃-trans (P p) U◂V)

  stable : {a b : S} {U V : 𝕌} →
    a ◃ U → b ◃ V → a ↓ b ◂ U ↓″ V
  stable (reflexivity a∈U) (reflexivity b∈V) (d≤a , d≤b) =
    reflexivity (( -, (a∈U , d≤a)) , ( -, (b∈V , d≤b)))
  stable (≤-left a≤c c◃U) b◃V = ◂-mono (↓-mono₁ a≤c) (stable c◃U b◃V)
  stable {a} {b} {U} {V} (≤-infinity {b = c} {i} a≤c Cci↓a◂U) b◃V {d} (d≤a , d≤b) =
    let d≤c = ≤-trans d≤a a≤c
    in  ≤-infinity {i = i} d≤c Cci↓d◂U↓V
    where
      Cci↓a↓b◂U↓V : (C c i) ↓′ a ↓′ b ◂ U ↓″ V
      Cci↓a↓b◂U↓V x∈Cci↓a↓b =
        let ((y , y∈Cci↓a , x≤y) , x≤b) = x∈Cci↓a↓b
            y◃U = Cci↓a◂U y∈Cci↓a
            y↓b◂U↓V = stable y◃U b◃V
            x↓b◂U↓V = ◂-mono (↓-mono₁ x≤y) y↓b◂U↓V
        in  x↓b◂U↓V (≤-refl , x≤b)
      ↓Cci∩a↓b◂U↓V : ↓₁ (C c i) ∩ (a ↓ b) ◂ U ↓″ V
      ↓Cci∩a↓b◂U↓V = ◂-mono (↓′-assoc (C c i) a b) Cci↓a↓b◂U↓V
      Cci↓d◂U↓V : (C c i) ↓′ d ◂ U ↓″ V
      Cci↓d◂U↓V (x∈↓Cci , x≤d) =
        let x≤a = ≤-trans x≤d d≤a
            x≤b = ≤-trans x≤d d≤b
        in  ↓Cci∩a↓b◂U↓V (x∈↓Cci , x≤a , x≤b)
  stable (positivity P) b◃V (d≤a , d≤b) =
    positivity (λ p → stable (P (monotonicity-≤ d≤a p)) b◃V (d≤a , d≤b))
  stable a◃U (≤-left b≤c c◃V) = ◂-mono (↓-mono₂ b≤c) (stable a◃U c◃V)
  stable {a} {b} {U} {V} a◃U (≤-infinity {b = c} {i} b≤c Cci↓b◂V) {d} (d≤a , d≤b) =
    let d≤c = ≤-trans d≤b b≤c
    in  ≤-infinity {i = i} d≤c Cci↓d◂U↓V
    where
      Cci↓b↓a◂U↓V : (C c i) ↓′ b ↓′ a ◂ U ↓″ V
      Cci↓b↓a◂U↓V x∈Cci↓b↓a =
        let ((y , y∈Cci↓b , x≤y) , x≤a) = x∈Cci↓b↓a
            y◃V = Cci↓b◂V y∈Cci↓b
            a↓y◂U↓V = stable a◃U y◃V
            a↓x◂U↓V = ◂-mono (↓-mono₂ x≤y) a↓y◂U↓V
        in  a↓x◂U↓V (x≤a , ≤-refl)
      ↓Cci∩b↓a◂U↓V : ↓₁ (C c i) ∩ (b ↓ a) ◂ U ↓″ V
      ↓Cci∩b↓a◂U↓V = ◂-mono (↓′-assoc (C c i) b a) Cci↓b↓a◂U↓V
      Cci↓d◂U↓V : (C c i) ↓′ d ◂ U ↓″ V
      Cci↓d◂U↓V (x∈↓Cci , x≤d) =
        let x≤a = ≤-trans x≤d d≤a
            x≤b = ≤-trans x≤d d≤b
        in  ↓Cci∩b↓a◂U↓V (x∈↓Cci , x≤b , x≤a)
  stable a◃U (positivity P) (d≤a , d≤b) =
    positivity (λ p → stable a◃U (P (monotonicity-≤ d≤b p)) (d≤a , d≤b))

  ≤-right : {a : S} {U V : 𝕌} →
    a ◃ U → a ◃ V → a ◃ U ↓″ V
  ≤-right a◃U a◃V = ◃-trans a◃a↓a (stable a◃U a◃V)

  monotonicity : {a : S} {U : 𝕌} →
    a ◃ U → Pos a → Pos′ U
  monotonicity (reflexivity a∈U) p = -, (a∈U , p)
  monotonicity (≤-left a≤b b◃U) p = monotonicity b◃U (monotonicity-≤ a≤b p)
  monotonicity (≤-infinity {i = i} a≤b Cbi↓a◂U) p =
    let P = monotonicity-≤-infinity i a≤b p
        (x , (x∈Cbi↓a , q)) = P
        x◃U = Cbi↓a◂U x∈Cbi↓a
    in  monotonicity x◃U q
  monotonicity (positivity P) p = monotonicity (P p) p

  formalTopology : FormalTopology
  formalTopology = record
    {
      _◃_ = _◃_;
      Pos = Pos;
      ◃-refl = _◃_.reflexivity;
      ◃-trans = ◃-trans;
      ≤-left = _◃_.≤-left;
      ≤-right = ≤-right;
      monotonicity = monotonicity;
      positivity = _◃_.positivity
    }
