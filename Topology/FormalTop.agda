{-# OPTIONS --without-K --safe #-}

-- T. Coquand, G. Sambin, J. Smith and S. Valentini,
-- Inductively generated formal topologies

open import Data.Product using (Σ; _×_; -,_; _,_)
open import Level using (Level; suc; _⊔_)
open import Relation.Binary using (Preorder; REL; Rel)
open import Relation.Unary using (Pred; _∈_; _∩_; _⊆_)

module Topology.FormalTop {ℓ : Level} (preorder : Preorder ℓ ℓ ℓ)
  (μ π : Level) where

open Preorder preorder public
  renaming (Carrier to S; _∼_ to _≤_; refl to ≤-refl; trans to ≤-trans)

-- Coverings

𝕌 : Set (suc ℓ)
𝕌 = Pred S ℓ

-- Refining operators

↓₁ : 𝕌 → 𝕌
↓₁ U = λ c → Σ S λ u → u ∈ U × c ≤ u

infixl 14 _↓_
_↓_ : S → S → 𝕌
a ↓ b = λ c → c ≤ a × c ≤ b

infixl 14 _↓′_
_↓′_ : 𝕌 → S → 𝕌
U ↓′ a = ↓₁ U ∩ (λ c → c ≤ a)

infixl 14 _↓″_
_↓″_ : 𝕌 → 𝕌 → 𝕌
U ↓″ V = ↓₁ U ∩ ↓₁ V

↓-mono₁ : {a b c : S} →
  a ≤ c → a ↓ b ⊆ c ↓ b
↓-mono₁ a≤c (d≤a , d≤b) = (≤-trans d≤a a≤c , d≤b)

↓-mono₂ : {a b c : S} →
  b ≤ c → a ↓ b ⊆ a ↓ c
↓-mono₂ b≤c (d≤a , d≤b) = (d≤a , ≤-trans d≤b b≤c)

↓′-assoc : (U : 𝕌) → (a b : S) →
  ↓₁ U ∩ a ↓ b ⊆ (U ↓′ a) ↓′ b
↓′-assoc U a b ((y , y∈U , x≤y) , x≤a , x≤b) =
  ( -, (((y , y∈U , x≤y) , x≤a) , ≤-refl)) , x≤b

↓′-assoc′ : (U : 𝕌) (a b : S) →
  (U ↓′ a) ↓′ b ⊆ ↓₁ U ∩ a ↓ b
↓′-assoc′ U a b ((y , ((z , z∈U , y≤z) , y≤a) , x≤y) , x≤b) =
  let x≤z = ≤-trans x≤y y≤z
      x≤a = ≤-trans x≤y y≤a
  in  (z , z∈U , x≤z) , x≤a , x≤b

record FormalTopology : Set (suc (suc ℓ ⊔ μ ⊔ π)) where
  infix 12 _◃_
  infix 12 _◂_

  field
    _◃_ : REL S 𝕌 μ
    Pos : Pred S π

  _◂_ : Rel 𝕌 (ℓ ⊔ μ)
  _◂_ U V = {b : S} → b ∈ U → b ◃ V

  Pos′ : Pred 𝕌 (ℓ ⊔ π)
  Pos′ U = Σ S λ b → b ∈ U × Pos b

  field
    ◃-refl  : {a : S} {U : 𝕌} →
      a ∈ U → a ◃ U
    ◃-trans : {a : S} {U V : 𝕌} →
      a ◃ U → U ◂ V → a ◃ V
    ≤-left : {a b : S} {U : 𝕌} →
      a ≤ b → b ◃ U → a ◃ U
    ≤-right : {a : S} {U V : 𝕌} →
      a ◃ U → a ◃ V → a ◃ U ↓″ V
    monotonicity : {a : S} {U : 𝕌} →
      a ◃ U → Pos a → Pos′ U
    positivity : {a : S} {U : 𝕌} →
      (Pos a → a ◃ U) → a ◃ U
