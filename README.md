agda-real
=========

A formalisation of the real numbers in dependent type theory using the
Agda programming language.

Features:

- Constructive definitions based on two-sided Dedekind cuts

- Certified approximation of real numbers by rational numbers to
  arbitrary precision

- Code can be type-checked to prove correctness and can be compiled to
  efficiently run certifiably correct computations

- "Proof-of-concept" example: the square root function

- Includes a definition of a constructive topology on the real numbers


Author
------

Peter Bruin, <P.J.Bruin@math.leidenuniv.nl>


Prerequisites
-------------

- Agda (version 2.6.2 or later) including the Agda standard library
  (version 1.7 or later),
  <http://wiki.portal.chalmers.se/agda/pmwiki.php>


Usage
-----

Here is an example:

    $ make
    $ ./Test

This will output two intervals, the first approximating the positive
square root of 2 and the second approximating the golden ratio.


TODO
----

- Implement field operations for the real numbers

- Integration between the type of real numbers and the formal topology
  (real numbers as formal points)
