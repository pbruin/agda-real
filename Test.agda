{-# OPTIONS --guardedness #-}

module Test where

open import Data.Integer using (ℤ; +_)
open import Data.Product using (_,_)
open import Data.Rational hiding (show)
open import Data.Rational.Show
open import Data.String using (_++_; String)

open import IO using (run; putStr; Main)

open import Rational.Exp using (exp>0)
open import Rational.Lemmas

open import Real
open import Real.Addition using (_⊕_)
open import Real.Approx using (approx)
open import Real.Sqrt using (√)

√2 : ℝ
√2 = √ (ℚ→ℝ 2ℚ)

φ : ℝ
φ = ℚ→ℝ ½ ⊕ √ (ℚ→ℝ (+ 5 / 4))

test : ℝ → ℤ → String
test x k =
  let ε , 0<ε = exp>0 ½ _ k
      ((a , b) , _) = approx x ε 0<ε
  in  "(" ++ show a ++ ", " ++ show b ++ ")\n"

main : Main
main = run (putStr ("√2 ∈ " ++ test √2 (+ 16)
                    ++ "φ ∈ " ++ test φ (+ 10)))
