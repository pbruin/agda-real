{-# OPTIONS --without-K --safe #-}

-- Miscellaneous identities in a commutative ring

open import Algebra
open import Level

module CommutativeRing.Identities {c ℓ : Level} (A : CommutativeRing c ℓ) where

import Algebra.Properties.Ring
open import Data.Product
import Relation.Binary.Reasoning.Setoid

open CommutativeRing A

open Algebra.Properties.Ring (CommutativeRing.ring A)

module ≈-Reasoning = Relation.Binary.Reasoning.Setoid setoid

open ≈-Reasoning

x+y-y≈x : (x y : Carrier) → x + y - y ≈ x
x+y-y≈x x y = begin
  x + y - y   ≈⟨ +-assoc x y (- y) ⟩
  x + (y - y) ≈⟨ +-cong refl (proj₂ -‿inverse y) ⟩
  x + 0#      ≈⟨ proj₂ +-identity x ⟩
  x           ∎

x-y+y≈x : (x y : Carrier) → x - y + y ≈ x
x-y+y≈x x y = begin
  x - y + y     ≈⟨ +-assoc x (- y) y ⟩
  x + (- y + y) ≈⟨ +-cong refl (proj₁ -‿inverse y) ⟩
  x + 0#        ≈⟨ proj₂ +-identity x ⟩
  x             ∎

x+y+z≈x+z+y : (x y z : Carrier) → x + y + z ≈ x + z + y
x+y+z≈x+z+y x y z = begin
  x + y + z   ≈⟨ +-assoc x y z ⟩
  x + (y + z) ≈⟨ +-cong refl (+-comm y z) ⟩
  x + (z + y) ≈⟨ sym (+-assoc x z y) ⟩
  x + z + y   ∎

x*y*z≈x*z*y : (x y z : Carrier) → x * y * z ≈ x * z * y
x*y*z≈x*z*y x y z = begin
  x * y * z   ≈⟨ *-assoc x y z ⟩
  x * (y * z) ≈⟨ *-cong refl (*-comm y z) ⟩
  x * (z * y) ≈⟨ sym (*-assoc x z y) ⟩
  x * z * y   ∎

-- Note: this function is also in Properties.Ring,
-- but the proof below is more streamlined.
-‿+-comm′ : (x y : Carrier) → - x - y ≈ - (x + y)
-‿+-comm′ x y =
  let eq : x + y + (- y - x) ≈ 0#
      eq = begin
        x + y + (- y - x)   ≈⟨ +-assoc x y (- y - x) ⟩
        x + (y + (- y - x)) ≈⟨ +-cong refl (sym (+-assoc y (- y) (- x))) ⟩
        x + (y - y - x)     ≈⟨ +-cong refl (+-cong (proj₂ -‿inverse y) refl) ⟩
        x + (0# - x)        ≈⟨ +-cong refl (proj₁ +-identity (- x)) ⟩
        x - x               ≈⟨ proj₂ -‿inverse x ⟩
        0#                  ∎
  in  begin
        - x - y                         ≈⟨ +-comm (- x) (- y) ⟩
        - y - x                         ≈⟨ sym (proj₁ +-identity (- y - x)) ⟩
        0# + (- y - x)                  ≈⟨ +-cong (sym (proj₁ -‿inverse (x + y))) refl ⟩
        - (x + y) + (x + y) + (- y - x) ≈⟨ +-assoc (- (x + y)) (x + y) (- y - x) ⟩
        - (x + y) + (x + y + (- y - x)) ≈⟨ +-cong refl eq ⟩
        - (x + y) + 0#                  ≈⟨ proj₂ +-identity (- (x + y)) ⟩
        - (x + y)                       ∎

-- TODO: better names for the following functions?

x-[x-y]≈y : (x y : Carrier) → x - (x - y) ≈ y
x-[x-y]≈y x y = begin
    x - (x - y)     ≈⟨ +-cong refl (sym (-‿+-comm x ( - y))) ⟩
    x + (- x - - y) ≈⟨ +-cong refl (+-cong refl (-‿involutive y)) ⟩
    x + (- x + y)   ≈⟨ sym (+-assoc x (- x) y) ⟩
    x + - x + y     ≈⟨ +-cong (proj₂ -‿inverse x) refl ⟩
    0# + y          ≈⟨ proj₁ +-identity y ⟩
    y               ∎

z[y-x]≈zy-zx : (x y z : Carrier) → z * (y - x) ≈ z * y - z * x
z[y-x]≈zy-zx x y z = begin
  z * (y - x)     ≈⟨ proj₁ distrib z y (- x) ⟩
  z * y + z * - x ≈⟨ +-cong refl (-‿*-distribʳ z x) ⟩
  z * y - z * x   ∎

[y-x]z≈yz-xz : (x y z : Carrier) → (y - x) * z ≈ y * z - x * z
[y-x]z≈yz-xz x y z = begin
  (y - x) * z     ≈⟨ proj₂ distrib z y (- x) ⟩
  y * z + - x * z ≈⟨ +-cong refl (-‿*-distribˡ x z) ⟩
  y * z - x * z   ∎

y-x≈[z+y]-[z+x] : (x y z : Carrier) → y - x ≈ (z + y) - (z + x)
y-x≈[z+y]-[z+x] x y z = begin
  y - x               ≈⟨ sym (proj₁ +-identity (y - x)) ⟩
  0# + (y - x)        ≈⟨ +-cong (sym (proj₂ -‿inverse z)) refl ⟩
  (z - z) + (y - x)   ≈⟨ +-assoc z (- z) (y - x) ⟩
  z + (- z + (y - x)) ≈⟨ +-cong refl (sym (+-assoc (- z) y (- x))) ⟩
  z + (- z + y - x)   ≈⟨ +-cong refl (+-cong (+-comm (- z) y) refl) ⟩
  z + (y - z - x)     ≈⟨ +-cong refl (+-assoc y (- z) (- x)) ⟩
  z + (y + (- z - x)) ≈⟨ sym (+-assoc z y (- z - x)) ⟩
  z + y + (- z - x)   ≈⟨ +-cong refl (-‿+-comm z x) ⟩
  z + y - (z + x)     ∎

y-x≈[y+z]-[x+z] : (x y z : Carrier) → y - x ≈ (y + z) - (x + z)
y-x≈[y+z]-[x+z] x y z = begin
  y - x                ≈⟨ sym (proj₂ +-identity (y - x)) ⟩
  y - x + 0#           ≈⟨ +-cong refl (sym (proj₂ -‿inverse z)) ⟩
  y - x + (z - z)      ≈⟨ sym (+-assoc (y - x) z (- z)) ⟩
  y - x + z - z        ≈⟨ +-cong (+-assoc y (- x) z) refl ⟩
  y + ((- x) + z) - z  ≈⟨ +-cong (+-cong refl (+-comm (- x) z)) refl ⟩
  y + (z - x) - z      ≈⟨ +-cong (sym (+-assoc y z (- x))) refl ⟩
  y + z - x - z        ≈⟨ +-assoc (y + z) (- x) (- z) ⟩
  y + z + (- x - z)    ≈⟨ +-cong refl (-‿+-comm x z) ⟩
  y + z - (x + z)      ∎

[x-y]+[y+z]≈x+z : (x y z : Carrier) → x - y + (y + z) ≈ x + z
[x-y]+[y+z]≈x+z x y z = begin
  x - y + (y + z)     ≈⟨ sym (+-assoc (x - y) y z) ⟩
  (x - y + y) + z     ≈⟨ +-cong (x-y+y≈x x y) refl ⟩
  x + z               ∎

[x+y]+[-y+z]≈x+z : (x y z : Carrier) → x + y + (- y + z) ≈ x + z
[x+y]+[-y+z]≈x+z x y z = begin
  x + y + (- y + z)   ≈⟨ sym (+-assoc (x + y) (- y) z) ⟩
  (x + y) - y + z     ≈⟨ +-cong (x+y-y≈x x y) refl ⟩
  x + z               ∎
