{-# OPTIONS --without-K --safe #-}

module Rational.Lemmas where

open import Data.Integer using (+_; -[1+_]; -<+; +<+)
open import Data.Nat as ℕ using (z≤n; s≤s; suc)
open import Data.Product
open import Data.Sum
open import Data.Rational
open import Data.Rational.Properties
open import Function
open import Relation.Binary
open import Relation.Binary.PropositionalEquality

2ℚ : ℚ
2ℚ = + 2 / 1

-1ℚ<0ℚ : - 1ℚ < 0ℚ
-1ℚ<0ℚ = *<* -<+

0ℚ<1ℚ : 0ℚ < 1ℚ
0ℚ<1ℚ = *<* (+<+ (s≤s z≤n))

x<y∨y≤x : (x y : ℚ) → x < y ⊎ y ≤ x
x<y∨y≤x x y = case <-cmp y x of λ {
    (tri< y<x _ _) → inj₂ (<⇒≤ y<x);
    (tri≈ _ y≡x _) → inj₂ (≤-reflexive y≡x);
    (tri> _ _ x<y) → inj₁ x<y
  }

x<y⇒0<y-x : {x y : ℚ} → x < y → 0ℚ < y - x
x<y⇒0<y-x {x} {y} x<y =
  let y-y<y-x = +-monoʳ-< y (neg-antimono-< x<y)
  in  proj₂ <-resp-≡ (+-inverseʳ y) y-y<y-x

0<x⇒y<x+y : {x y : ℚ} → 0ℚ < x → y < x + y
0<x⇒y<x+y {x} {y} 0<x = <-respˡ-≡ (+-identityˡ y) (+-monoˡ-< y 0<x)

0<x⇒y<y+x : {x y : ℚ} → 0ℚ < x → y < y + x
0<x⇒y<y+x {x} {y} 0<x = <-respˡ-≡ (+-identityʳ y) (+-monoʳ-< y 0<x)

0<x⇒y-x<y : {x y : ℚ} → 0ℚ < x → y - x < y
0<x⇒y-x<y {x} {y} 0<x =
  <-respʳ-≡ (+-identityʳ y) (+-monoʳ-< y (neg-antimono-< 0<x))

+-pos : (x y : ℚ) → Positive x → Positive y → Positive (x + y)
+-pos (mkℚ (+ suc a) b _) (mkℚ (+ suc c) d _) _ _ =
  normalize-pos (suc a ℕ.* suc d ℕ.+ suc c ℕ.* suc b) (suc b ℕ.* suc d) _

+-pos-nonNeg : (x y : ℚ) → Positive x → NonNegative y → Positive (x + y)
+-pos-nonNeg (mkℚ (+ suc a) b _) (mkℚ (+ 0) d _) _ _ =
  normalize-pos ((suc a ℕ.* suc d) ℕ.+ 0) (suc b ℕ.* suc d) _
+-pos-nonNeg (mkℚ (+ suc a) b _) (mkℚ (+ suc c) d _) _ _ =
  normalize-pos ((suc a ℕ.* suc d) ℕ.+ suc c ℕ.* suc b) (suc b ℕ.* suc d) _

+-nonNeg-pos : (x y : ℚ) → NonNegative x → Positive y → Positive (x + y)
+-nonNeg-pos (mkℚ (+ 0) b _) (mkℚ (+ suc c) d _) _ _ =
  normalize-pos (suc c ℕ.* suc b) (suc b ℕ.* suc d) _
+-nonNeg-pos (mkℚ (+ suc a) b _) (mkℚ (+ suc c) d _) _ _ =
  normalize-pos ((suc a ℕ.* suc d) ℕ.+ suc c ℕ.* suc b) (suc b ℕ.* suc d) _

+-nonNeg : (x y : ℚ) → NonNegative x → NonNegative y → NonNegative (x + y)
+-nonNeg (mkℚ (+ 0) b _) (mkℚ (+ 0) d _) _ _ =
  normalize-nonNeg 0 (suc b ℕ.* suc d)
+-nonNeg (mkℚ (+ 0) b _) (mkℚ (+ suc c) d _) _ _ =
  normalize-nonNeg (suc c ℕ.* suc b) (suc b ℕ.* suc d)
+-nonNeg (mkℚ (+ suc a) b _) (mkℚ (+ 0) d _) _ _ =
  normalize-nonNeg ((suc a ℕ.* suc d) ℕ.+ 0) (suc b ℕ.* suc d)
+-nonNeg (mkℚ (+ suc a) b _) (mkℚ (+ suc c) d _) _ _ =
  normalize-nonNeg ((suc a ℕ.* suc d) ℕ.+ suc c ℕ.* suc b) (suc b ℕ.* suc d)

*-pos : (x y : ℚ) → Positive x → Positive y → Positive (x * y)
*-pos (mkℚ (+ suc a) b _) (mkℚ (+ suc c) d _) _ _ =
  normalize-pos (suc a ℕ.* suc c) (suc b ℕ.* suc d) _

x²-nonNeg : (x : ℚ) → NonNegative (x * x)
x²-nonNeg (mkℚ (+ 0) b _) =
  normalize-nonNeg 0 (suc b ℕ.* suc b)
x²-nonNeg (mkℚ (+ suc a) b _) =
  normalize-nonNeg (suc a ℕ.* suc a) (suc b ℕ.* suc b)
x²-nonNeg (mkℚ -[1+ a ] b _) =
  normalize-nonNeg (suc a ℕ.* suc a) (suc b ℕ.* suc b)

0≤x² : (x : ℚ) → 0ℚ ≤ x * x
0≤x² x = nonNegative⁻¹ (x²-nonNeg x)

⊔-pos : (x y : ℚ) → Positive x → Positive y → Positive (x ⊔ y)
⊔-pos x y p q with ⊔-sel x y
... | inj₁ x⊔y≡x = subst Positive (sym x⊔y≡x) p
... | inj₂ x⊔y≡y = subst Positive (sym x⊔y≡y) q

⊓-pos : (x y : ℚ) → Positive x → Positive y → Positive (x ⊓ y)
⊓-pos x y p q with ⊓-sel x y
... | inj₁ x⊓y≡x = subst Positive (sym x⊓y≡x) p
... | inj₂ x⊓y≡y = subst Positive (sym x⊓y≡y) q

⊔-lub-< : {x y t : ℚ} → x < t → y < t → x ⊔ y < t
⊔-lub-< {x} {y} x<t y<t with ⊔-sel x y
... | inj₁ x⊔y≡x = <-respˡ-≡ (sym x⊔y≡x) x<t
... | inj₂ x⊔y≡x = <-respˡ-≡ (sym x⊔y≡x) y<t

⊓-glb-< : {x y t : ℚ} → t < x → t < y → t < x ⊓ y
⊓-glb-< {x} {y} t<x t<y with ⊓-sel x y
... | inj₁ x⊓y≡x = <-respʳ-≡ (sym x⊓y≡x) t<x
... | inj₂ x⊓y≡x = <-respʳ-≡ (sym x⊓y≡x) t<y

inverse-pos : (x : ℚ) → Positive x →
              Σ ℚ λ y → Positive y × y * x ≡ 1ℚ × x * y ≡ 1ℚ
inverse-pos x@(mkℚ (+ suc _) _ _) 0<x =
  1/ x , pos⇒1/pos x 0<x , *-inverseˡ x , *-inverseʳ x

divide-pos : (x y : ℚ) → Positive x → Positive y →
             Σ ℚ λ z → Positive z × z * y ≡ x
divide-pos x y 0<x 0<y =
  let (y⁻¹ , 0<y⁻¹ , y⁻¹y≡1 , yy⁻¹≡1) = inverse-pos y 0<y
      z = x * y⁻¹
      eq : z * y ≡ x
      eq = begin
        x * y⁻¹ * y   ≡⟨ *-assoc x y⁻¹ y ⟩
        x * (y⁻¹ * y) ≡⟨ cong (x *_) y⁻¹y≡1 ⟩
        x * 1ℚ        ≡⟨ *-identityʳ x ⟩
        x             ∎
      0<z = *-pos x y⁻¹ 0<x 0<y⁻¹
  in  z , 0<z , eq
  where open ≡-Reasoning
