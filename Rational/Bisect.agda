{-# OPTIONS --without-K --safe #-}

module Rational.Bisect where

open import Algebra using (CommutativeRing)
open import Data.Product
open import Data.Rational
open import Data.Rational.Properties
open import Relation.Binary.PropositionalEquality
  using (_≡_; refl; sym; trans; module ≡-Reasoning)

open import Rational.Lemmas

open CommutativeRing +-*-commutativeRing
  using (+-cong; *-cong; distrib)

open import Algebra.Properties.Ring (CommutativeRing.ring +-*-commutativeRing)
  using (-‿+-comm; -‿*-distribʳ)

open import CommutativeRing.Identities +-*-commutativeRing
  using ([x+y]+[-y+z]≈x+z)

private

  x+x≡2x : (x : ℚ) → x + x ≡ 2ℚ * x
  x+x≡2x x =
    let x≡1x = sym (proj₁ *-identity x)
    in  trans (+-cong x≡1x x≡1x) (sym (proj₂ distrib x 1ℚ 1ℚ))

  ½*[x+x]≡x : (x : ℚ) → ½ * (x + x) ≡ x
  ½*[x+x]≡x x = begin
      ½ * (x + x)  ≡⟨ *-cong {½} refl (x+x≡2x x) ⟩
      ½ * (2ℚ * x) ≡⟨ sym (*-assoc ½ 2ℚ x) ⟩
      ½ * 2ℚ * x   ≡⟨⟩
      1ℚ * x       ≡⟨ proj₁ *-identity x ⟩
      x            ∎
    where open ≡-Reasoning

  lemma : (x : ℚ) → 2ℚ * (½ * x) ≡ x
  lemma x = begin
      2ℚ * (½ * x) ≡⟨ sym (*-assoc 2ℚ ½ x) ⟩
      2ℚ * ½ * x   ≡⟨⟩
      1ℚ * x       ≡⟨ proj₁ *-identity x ⟩
      x            ∎
    where open ≡-Reasoning

  lemma′ : (x y : ℚ) → 2ℚ * - (½ * (x + y)) ≡ - x - y
  lemma′ x y = begin
      2ℚ * - (½ * (x + y))   ≡⟨ -‿*-distribʳ 2ℚ (½ * (x + y)) ⟩
      - (2ℚ * (½ * (x + y))) ≡⟨ -‿cong (lemma (x + y)) ⟩
      - (x + y)              ≡⟨ sym (-‿+-comm x y) ⟩
      - x - y                ∎
    where open ≡-Reasoning

bisect : {a b : ℚ} → a < b →
         Σ ℚ λ c → a < c × c < b ×
                   2ℚ * (c - a) ≡ b - a × 2ℚ * (b - c) ≡ b - a
bisect {a} {b} a<b =
  let c = ½ * (b + a)
      a+a<b+a = +-monoˡ-< a a<b
      b+a<b+b = +-monoʳ-< b a<b
      a<c = proj₂ <-resp-≡ (½*[x+x]≡x a) (*-monoʳ-<-pos ½ _ a+a<b+a)
      c<b = proj₁ <-resp-≡ (½*[x+x]≡x b) (*-monoʳ-<-pos ½ _ b+a<b+b)
      2[c-a]≡b-a = begin
        2ℚ * (c - a)                  ≡⟨ proj₁ distrib 2ℚ c (- a) ⟩
        2ℚ * (½ * (b + a)) + 2ℚ * - a ≡⟨ +-cong (lemma (b + a))
                                                (sym (x+x≡2x (- a))) ⟩
        b + a + (- a - a)             ≡⟨ [x+y]+[-y+z]≈x+z b a (- a) ⟩
        b - a                         ∎
      2[b-c]≡b-a = begin
        2ℚ * (b - c)                  ≡⟨ proj₁ distrib 2ℚ b (- c) ⟩
        2ℚ * b + 2ℚ * - (½ * (b + a)) ≡⟨ +-cong (sym (x+x≡2x b)) (lemma′ b a) ⟩
        b + b + (- b - a)             ≡⟨ [x+y]+[-y+z]≈x+z b b (- a) ⟩
        b - a                         ∎
  in  c , a<c , c<b , 2[c-a]≡b-a , 2[b-c]≡b-a
  where open ≡-Reasoning

intermediate : {a b : ℚ} → a < b → Σ ℚ λ c → a < c × c < b
intermediate a<b =
  let (c , a<c , c<b , _ , _) = bisect a<b
  in  (c , a<c , c<b)
