{-# OPTIONS --without-K --safe #-}

-- Powering in ℚ

module Rational.Exp where

open import Data.Integer using (ℤ; +_; -[1+_])
open import Data.Nat using (ℕ)
open import Data.Product
open import Data.Rational
open import Data.Rational.Properties using (pos⇒1/pos)

open import Rational.Lemmas using (*-pos)

private

  exp>0-ℕ : (x : ℚ) → Positive x → ℕ → Σ ℚ λ y → Positive y
  exp>0-ℕ x 0<x 0 = 1ℚ , _
  exp>0-ℕ x 0<x (ℕ.suc n) =
    let (y , 0<y) = exp>0-ℕ x 0<x n
    in  y * x , *-pos y x 0<y 0<x

exp>0 : (x : ℚ) → Positive x → ℤ → Σ ℚ λ y → Positive y
exp>0 x 0<x (+ n) = exp>0-ℕ x 0<x n
exp>0 x 0<x -[1+ n ] =
  let (y , 0<y) = exp>0-ℕ x 0<x (ℕ.suc n)
      z = 1/ y
      0<z = pos⇒1/pos y 0<y
  in  z , 0<z
